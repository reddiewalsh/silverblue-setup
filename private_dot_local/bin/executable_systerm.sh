#!/usr/bin/bash

# Using this script inside a toolbox will pop the toolbox off the stack and launch a gnome terminal with the host profile.
# This is necessary as I set my default terminal as a toolbox which makes the host terminal inaccessable.

printf "\033]777;container;pop;;\033\\" && \
	flatpak-spawn --host gnome-terminal --window-with-profile=host
