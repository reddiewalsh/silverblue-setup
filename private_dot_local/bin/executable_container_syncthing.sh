#!/usr/bin/bash

for dir in ".config/syncthing" "Sync" "Documents" "Notes" "Games"
do
	if [[ ! -d {$HOME}/$dir} ]]; then
		mkdir -p ~/$dir
	fi
	podman unshare chown 1000:1000 -R $HOME/$dir
done

podman run -d \
	--name=syncthing \
	--hostname=syncthing-${HOSTNAME} \
	--userns=keep-id \
	--network=host \
	-e PUID=1000 \
	-e PGID=1000 \
	-v $HOME/.config/syncthing:/var/syncthing/config:z,U \
	-v $HOME/Documents:/var/syncthing/Documents:z,U \
	-v $HOME/Sync:/var/syncthing/Sync:z,U \
	-v $HOME/Notes:/var/syncthing/Notes:z,U \
	-v $HOME/Games:/var/syncthing/Games:z,U \
	syncthing/syncthing:latest &&\
podman generate systemd syncthing > $HOME/.config/systemd/user/container-syncthing.service
